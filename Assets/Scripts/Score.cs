﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public void SetText(string text)
    {
        GetComponent<Text>().text = text;
    }
}
