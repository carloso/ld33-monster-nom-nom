﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private GameObject GameUI;
    private Score scoreText;
    private int score = 0;
    private bool playing = true;
    private FoodSpawner spawner;
    private GameObject panel;
    private Image flashImage;
    private float flashAlpha = 1.2F;
    private bool flash = false;
    private bool exploded = false;

	void Start () {
        GameUI = GameObject.FindGameObjectWithTag("UI");
        panel = GameObject.FindGameObjectWithTag("Retry");
        panel.SetActive(false);
        scoreText = GameUI.GetComponentInChildren<Score>();
        spawner = GameObject.FindGameObjectWithTag("Respawn").GetComponent<FoodSpawner>();
        flashImage = GameObject.FindGameObjectWithTag("Flash").GetComponentInChildren<Image>();
	}

    void Update()
    {
        if (flash)
        {
            flashAlpha -= Time.deltaTime;
            flashImage.color = new Color(1, 1, 1, flashAlpha);
            if (flashAlpha <= 0)
            {
                flashImage.color = new Color(1, 1, 1, 0);
                flashImage.enabled = false;
                flash = false;
                panel.SetActive(true);
                flashAlpha = 1.2F;
            }
        }
    }

    public void Explode()
    {
        if (exploded)
            return;

        GameObject.FindGameObjectWithTag("MainCamera").ShakePosition(new Vector3(1, 1, 1), 1, 0);
        flash = true;
        flashImage.color = Color.white;
        flashImage.enabled = true;
        exploded = true;

        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInput>().Smoke();
    }
	
    public void ScorePoint(int points = 1)
    {
        if (!playing)
            return;

        score += points;
        spawner.EatFood();

        scoreText.SetText(string.Format("Score: {0}", score));
    }

    public void BlowUp()
    {
        scoreText.SetText(string.Format("Final Score: {0}", score));
        spawner.Stop();
        playing = false;
    }

    public void ResetLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void GoToMainMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
