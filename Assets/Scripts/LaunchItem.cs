﻿using UnityEngine;
using System.Collections;

public class LaunchItem : MonoBehaviour {

    public Vector2 force;

	void Start () {
        Invoke("Destroy", 5);
	}

    public void Destroy()
    {
        Destroy(gameObject);
    }

    void OnEnable()
    {
        var rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddForce(force, ForceMode2D.Impulse);

        int direction = Random.Range(0, 2) == 1 ? 1 : -1;
        rb2d.AddTorque(Random.Range(0.2F, 1.5F) * direction, ForceMode2D.Impulse);
    }
}
