﻿using UnityEngine;
using System.Collections;

public class IntroCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.MoveTo(new Vector3(0, -1.2F, -10), 0.82F, 0.05F, EaseType.easeOutBack);
	}
}
