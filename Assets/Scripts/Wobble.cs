﻿using UnityEngine;
using System.Collections;

public class Wobble : MonoBehaviour {

    public float scaleStrength = 0.015F;
    public float moveStrength = 0.2F;

    public EaseType scaleEase = EaseType.easeInOutBounce;
    public EaseType moveEase = EaseType.easeInOutSine;

	void Start () {
        Wiggle();
	}

    void Wiggle()
    {
        gameObject.ScaleAdd(randomVector(scaleStrength), 2, 0, scaleEase, LoopType.pingPong);
        gameObject.MoveAdd(randomVector(moveStrength), 2, 0, moveEase, LoopType.pingPong);

        Invoke("Wiggle", 4);
    }

    Vector3 randomVector(float strength)
    {
        return new Vector3(randomFloat(strength), randomFloat(strength), 0);
    }

    float randomFloat(float strength)
    {
        return Random.Range(-1  * strength, strength);
    }
}