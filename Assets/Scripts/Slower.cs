﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slower : MonoBehaviour {

    List<GameObject> inside = new List<GameObject>();

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag.Contains("Bad"))
            Time.timeScale = 0.4F;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Time.timeScale = 1F;
    }
}
