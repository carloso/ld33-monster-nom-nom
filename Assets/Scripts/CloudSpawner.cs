﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CloudSpawner : MonoBehaviour {

    public float SpawnFrequency = 4;
    public float SpawnDelta = 1.5F;
    public List<GameObject> Clouds;

    private BoxCollider2D spawnAreaCollider2D;

	// Use this for initialization
	void Start () {
        spawnAreaCollider2D = GetComponent<BoxCollider2D>();

        Invoke("Spawn", RandomTime());
	}
	
    void Spawn()
    {
        var toSpawn = Clouds[Random.Range(0, Clouds.Count)];

        Instantiate(toSpawn, RandomPoint(), Quaternion.identity);

        Invoke("Spawn", RandomTime());
    }

    float RandomTime()
    {
        return Random.Range(-1 * SpawnDelta, SpawnDelta) + SpawnFrequency;
    }

    Vector3 RandomPoint()
    {
        var half = spawnAreaCollider2D.size.y / 2F;

        return new Vector3(transform.position.x, transform.position.y + Random.Range(half * -1F, half), 0);
    }
}
