﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(randomForce(), ForceMode2D.Force);
        GetComponent<SpriteRenderer>().sortingOrder = Random.Range(3, 7);
    }

    Vector2 randomForce()
    {
        return new Vector2(-1F * Random.Range(0.001F, 0.005F), 0);
    }
}
