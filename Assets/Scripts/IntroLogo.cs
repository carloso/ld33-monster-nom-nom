﻿using UnityEngine;
using System.Collections;

public class IntroLogo : MonoBehaviour {

    public Canvas canvas;

	// Use this for initialization
	void Start () {
        gameObject.MoveTo(new Vector3(0, 0, 0), 0.8F, 0.85F, EaseType.easeOutElastic);
        Invoke("Button", 1.5F);
	}

    public void Button()
    {
        canvas.enabled = true;
    }
}
