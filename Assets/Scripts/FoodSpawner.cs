﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodSpawner : MonoBehaviour {

    private GameObject goodFood;
    private GameObject badFood;
    private int foodsEaten = 0;
    private List<float> plusDifficulty = new List<float>()
    {
        0,
        0.02F,
        0.03F,
        0.06F,
        0.08F,
        0.1F,
        0.15F,
        0.17F,
        0.18F,
        0.2F,
        0.21F,
        0.22F,
        0.24F,
        0.25F,
        0.3F,
        0.32F,
        0.33F
    };

    public float difficulty = .5F;

	// Use this for initialization
	void Start () {
        goodFood = Resources.Load<GameObject>("Good");
        badFood = Resources.Load<GameObject>("Bad");


        // .2 hard - .35 ok - .5 easy
        Invoke("Tick", difficulty + 2);
	}

    float difficultyDifference()
    {
        int index = 0;

        if (foodsEaten > 10)
            index++;

        if (foodsEaten > 20)
            index++;

        if (foodsEaten > 30)
            index++;

        if (foodsEaten > 40)
            index++;

        if (foodsEaten > 50) // 1F
            index++;

        if (foodsEaten > 70)
            index++;

        if (foodsEaten > 80)
            index++;

        if (foodsEaten > 90)
            index++;

        if (foodsEaten > 100) // 2F
            index++;

        if (foodsEaten > 140)
            index++;

        if (foodsEaten > 180)
            index++;

        if (foodsEaten > 220)
            index++;

        if (foodsEaten > 280)
            index++;

        if (foodsEaten > 350) // 3F
            index++;

        if (foodsEaten > 400)
            index++;

        if (foodsEaten > 500)
            index++;

        return plusDifficulty[index];
    }

    public void Tick()
    {
        var x = Random.Range(-1, 3);

        if (x > 0)
        {
            Instantiate(goodFood);
        }

        if (x < 0)
        {
            Instantiate(badFood);
        }

        Invoke("Tick", difficulty - difficultyDifference());
    }

    public void EatFood()
    {
        foodsEaten++;
    }

    public void Stop()
    {
        CancelInvoke("Tick");
    }
}
