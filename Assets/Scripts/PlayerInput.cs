﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private Color originalColor;
    private GameManager manager;
    private bool isPlaying = true;
    private bool isMouthOpen = false;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
        animator = GetComponent<Animator>();

        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>();
    }
	
    void Update()
    {
        animator.SetBool("IsOpen", isMouthOpen);
        animator.SetBool("IsExploding", !isPlaying);

        if (!isPlaying)
            return;

        isMouthOpen = Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        GameObject food = coll.gameObject;

        handleEat(food.tag.Contains("Bad"), food);
    }

    void handleEat(bool isBad, GameObject food)
    {
        if (!isPlaying)
        {
            gameObject.PunchScale(new Vector3(0.3F, -0.3F, 0), 0.33F, 0);

            return;
        }

        if (isMouthOpen)
        {
            Destroy(food);
            Time.timeScale = 1;

            if (isBad)
            {
                manager.BlowUp();
                gameObject.ShakePosition(new Vector3(0.1F, 0.1F, 0), 1, 0);
                isPlaying = false;
            }
            else
            {
                manager.ScorePoint();
                
                gameObject.PunchScale(new Vector3(0.2F, 0.3F, 0), 0.33F, 0);
            }
        }
        else
        {
            gameObject.PunchScale(new Vector3(0.3F, -0.3F, 0), 0.33F, 0);
        }
    }

    public void Explode()
    {
        manager.Explode();
    }

    public void Smoke()
    {
        GetComponentInChildren<ParticleSystem>().Play();
    }
}
